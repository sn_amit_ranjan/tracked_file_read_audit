doIt();
// query the track config file table
// iterate through all of them for them atch in file content and match any of the tag in config file debug the result and exit the loop

//Tomacat : Password, connectionPassword, secret, keyPass, keystorePass, SSLPassword
//mysql : Password
//sqlserver: PublicKeyToken
//Apache
//Oracle: token, pin
//Tibco: com.tibco.trinity.runtime.core.provider.credential.keystore.truststore.keyStorePassword, com.tibco.trinity.runtime.core.provider.identity.subject.keyPassword
//IIS
// Table that stores trcked file is 'cmdb_ci_config_file_tracked', column in question is 'File Content'
//sys_class_name
function doIt() {
   var keyWordArray = ['password', 'connectionpassword', 'secret', 'keypass', 'keystorepass', 'sslpassword', 'publickeytoken', 'token', 'pin', 'authldapbindpassword', 'key', 'keyStorePassword', 'keyPassword'];
   var trackedTableName = 'cmdb_ci_config_file_tracked';
   var propertyTableName = 'sys_properties';

   var trackedColumnName = 'content';
   var propertyColumnName = 'name';
   var fileNameColumnName = 'name';

   var trackedProperty = 'glide.discovery.enable_file_tracking';

   var propertyColumnValue = 'value';
   var isFileTrackingEnabled = false;
   var tokenFound = false;
   var noOfFileTracked = 0;
   var related_ci_name = '';
   
   var file_name = '';
   var nthFile = 0;
   var whichTokenFound = '';
   var ci_class_name = '';

   var propertyRecord = new GlideRecord(propertyTableName); //gs.GetProperty(property_Name) is the better option than below code
   propertyRecord.addQuery(propertyColumnName, '=', trackedProperty);
   propertyRecord.query();
   if(propertyRecord.next())
         isFileTrackingEnabled = propertyRecord.getValue(propertyColumnValue);


   if(isFileTrackingEnabled.toString().equalsIgnoreCase("true")){

      var trackedRecord = new GlideRecord(trackedTableName);
      trackedRecord.query();
      noOfFileTracked = trackedRecord.getRowCount();
      nthFile = 0;
         while (trackedRecord.next()) {
               nthFile++;
               var fileContent = trackedRecord.getValue(trackedColumnName);
               related_ci_name = trackedRecord.related_ci.name;
               ci_class_name = trackedRecord.related_ci.sys_class_name;
               file_name = trackedRecord.getValue(fileNameColumnName);
               for(var indx = 0; indx < keyWordArray.length; indx++){
                  if (fileContent.toString().toLowerCase().indexOf(keyWordArray[indx]) > -1){
                     whichTokenFound = keyWordArray[indx];
                     tokenFound = true;
                     break;
                  }
               }
                  if(tokenFound.toString().equalsIgnoreCase("true"))
                     break;
         }
      }
gs.log("glide.discovery.enable_file_tracking :: "+isFileTrackingEnabled+", tokenFound :: "+tokenFound+ ", whichTokenFound :: "+whichTokenFound+", noOfFileTracked :: "+noOfFileTracked+
   ", related_ci :: "+related_ci_name+", ci_class_name :: "+ci_class_name+", file_name :: "+file_name+", nthFile :: "+nthFile);

}